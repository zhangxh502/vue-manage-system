FROM node:12.18.2 as builder

WORKDIR /data
ADD ./package.json /data/package.json
ADD ./package-lock.json /data/package-lock.json
RUN yarn config set registry https://registry.npm.taobao.org --global
RUN npm install

ADD . /data
RUN npm run build

FROM nginx
COPY --from=builder /data/dist /opt/vue-manage-system/
COPY nginx.conf /etc/nginx/conf.d/default.conf
